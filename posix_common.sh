#!/bin/env sh
#
# Useful functions portable across bash and zsh.

# Easy to use base colors for scripts.
#
# See logging functions below for examples.
#
# Only runs `tput` if session is interactive and
# TTY is assigned falling back to non-colored
# output seamlessly where it is not supported.
_colors()
{
    if test -t 1; then
        RED=$(tput setaf 1)
        YELLOW=$(tput setaf 3)
        CYAN=$(tput setaf 6)
        NORMAL=$(tput sgr0)
    else
        RED=''
        YELLOW=''
        CYAN=''
        NORMAL=''
    fi
}

# Easy to use solarized colors for scripts.
#
# http://ethanschoonover.com/solarized
#
# Only runs `tput` if session is interactive and
# TTY is assigned falling back to non-colored
# output seamlessly where it is not supported.
# Sets close linux console colors on failure.
#
# Ensure 256 color term is enabled.
_solarized()
{
    if tput setaf 1 &> /dev/null; then
        tput sgr0
        if [[ $(tput colors) -ge 256 ]] 2>/dev/null; then
          BASE03=$(tput setaf 234)
          BASE02=$(tput setaf 235)
          BASE01=$(tput setaf 240)
          BASE00=$(tput setaf 241)
          BASE0=$(tput setaf 244)
          BASE1=$(tput setaf 245)
          BASE2=$(tput setaf 254)
          BASE3=$(tput setaf 230)
          YELLOW=$(tput setaf 136)
          ORANGE=$(tput setaf 166)
          RED=$(tput setaf 160)
          MAGENTA=$(tput setaf 125)
          VIOLET=$(tput setaf 61)
          BLUE=$(tput setaf 33)
          CYAN=$(tput setaf 37)
          GREEN=$(tput setaf 64)
        else
          BASE03=$(tput setaf 8)
          BASE02=$(tput setaf 0)
          BASE01=$(tput setaf 10)
          BASE00=$(tput setaf 11)
          BASE0=$(tput setaf 12)
          BASE1=$(tput setaf 14)
          BASE2=$(tput setaf 7)
          BASE3=$(tput setaf 15)
          YELLOW=$(tput setaf 3)
          ORANGE=$(tput setaf 9)
          RED=$(tput setaf 1)
          MAGENTA=$(tput setaf 5)
          VIOLET=$(tput setaf 13)
          BLUE=$(tput setaf 4)
          CYAN=$(tput setaf 6)
          GREEN=$(tput setaf 2)
        fi
        BOLD=$(tput bold)
        RESET=$(tput sgr0)
    else
        # Linux console colors.
        # Not the Solarized values
        MAGENTA="\033[1;31m"
        ORANGE="\033[1;33m"
        GREEN="\033[1;32m"
        PURPLE="\033[1;35m"
        WHITE="\033[1;37m"
        BOLD=""
        RESET="\033[m"
    fi
}

# Print a red error message.
_error()
{
    _colors
    printf "${RED}[*] Error: ${NORMAL}$1"
    echo ""
}

# Print a yellow warning message.
_warning()
{
    _colors
    printf "${YELLOW}[*] Warning: ${NORMAL}$1"
    echo ""
}

# Print a cyan info message.
_info()
{
    _colors
    printf "${CYAN}[*] Info: ${NORMAL}$1"
    echo ""
}

# Print a horizontal rule the exact width of the term.
#
# Only executes if session is interactive and
# TTY is assigned has no effect where it is
# not supported.
_rule()
{
    if test -t 1; then
        printf -v _hr "%*s" $(tput cols) && echo ${_hr// /${1--}}
    fi
}

# Test if a command exists.
#
# Returns 0 if command exists, otherwise returns 1.
_command_exists()
{
    command -v "$@" > /dev/null 2>&1
}

# Ask user a yes or no question.
#
# Prefers using a graphical dialog box and falls back to
# text prompt if no supported command is found. Will
# continually prompt user until an acceptable answer is
# provided.
#
# In general, POSIX is very consistent, and in all cases
# where the return value is only used to signal success or
# failure, 0 means success.
#
# See the POSIX function definition for more information:
#   * http://www.unix.com/man-page/all/3posix/pthread_mutex_trylock/
_ask_yes_no()
{
    if $(_command_exists whiptail); then
        dialog_box="whiptail"
    elif $(_command_exists dialog); then
        dialog_box="dialog"
    elif $(_command_exists gdialog); then
        dialog_box="gdialog"
    elif $(_command_exists kdialog); then
        dialog_box="kdialog"
    else
        dialog_box=""
    fi

        dialog_box=""

    if [[ ! -z $dialog_box ]]; then
        sh -c "$dialog_box --yesno \"$1\" 0 0" 
        result="$?"
    else
        while true; do
            echo -n "$1? (y/n) "
            read answer
            case "$answer" in
                y | Y | yes | YES)
                    result=0
                    break
                    ;;
                n | N | no | NO)
                    result=1
                    break
                    ;;
            esac
        done
    fi

    return "$result"
}
